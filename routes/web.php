<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/about', function () {
    return view('about');
});

Route::get('/howtoorder', function () {
    return view('howtoorder');
});

Route::get('/index', 'ViewUserController@index');
Route::get('/products-grid', 'ViewUserController@products_grid');
Route::get('/category', 'ViewUserController@category');

Route::get('/index-2', function () {
    return view('index-2');
});

Route::get('/index-3', function () {
    return view('index-3');
});

Route::get('/index-4', function () {
    return view('index-4');
});

Route::get('/index-5', function () {
    return view('index-5');
});

Route::get('/index-6', function () {
    return view('index-6');
});

Route::get('/index-xmas', function () {
    return view('index-xmas');
});

Route::get('/index-rtl', function () {
    return view('index-rtl');
});


Route::get('/login', function () {
    return view('login');
});

Route::get('/contact', function () {
    return view('contact');
});

Route::get('/product', function () {
    return view('product');
});






