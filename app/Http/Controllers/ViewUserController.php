<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class ViewUserController extends Controller
{
	public function index() {
		$sliders = DB::select( DB::raw("SELECT * FROM slider"));
		$clients = DB::select( DB::raw("SELECT * FROM client ORDER BY 1 DESC LIMIT 6"));
		$kategoris = DB::select( DB::raw("SELECT * FROM kategori"));
		$statics = DB::select( DB::raw("SELECT * FROM statics"));
		$tops = DB::select( DB::raw("SELECT * FROM kategori ORDER BY 1 DESC LIMIT 3"));
		$produk1 = DB::select( DB::raw("SELECT * FROM produk WHERE kategori_id = 3"));
		
		
		return view('index', compact('sliders', 'clients', 'kategoris', 'statics', 'tops', 'produk1'));
	}

	public function products_grid() {
		$kategoris = DB::select( DB::raw("SELECT * FROM kategori"));
		$produk1 = DB::select( DB::raw("SELECT * FROM produk WHERE kategori_id = 3"));
		return view('products-grid', compact('kategoris', 'produk1'));
	}

	public function category() {
		$kategoris = DB::select( DB::raw("SELECT * FROM kategori"));
		return view('category', compact('kategoris'));
	}
    
}
